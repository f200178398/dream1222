﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootParabolaBubble_3 : MonoBehaviour
{
    public float _timer;
    public float shootTime=0.5f;

    public ParabolaBubblePool parabolaBubblePool;

    private void Start()
    {
        parabolaBubblePool = GameObject.FindGameObjectWithTag("ObjectPool").GetComponent<ParabolaBubblePool>();
    }
    private void FixedUpdate()
    {
        _timer += Time.deltaTime;
        if (_timer>shootTime)
        {
            parabolaBubblePool.ReUse(transform.position, transform.rotation);
            _timer = 0;
        }
    }
}
