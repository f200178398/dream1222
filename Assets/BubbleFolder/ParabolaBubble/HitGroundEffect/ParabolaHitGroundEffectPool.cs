﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParabolaHitGroundEffectPool : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void Awake()
    {

        for (int cnt = 0; cnt < initailSize; cnt++)
        {
            randombubbleIndex = Random.Range(0, prefabs.Length);
            GameObject go = Instantiate(prefabs[randombubbleIndex]) as GameObject;
            m_pool.Enqueue(go);
            go.SetActive(false);
        }
    }


    [Header("Inititate Settings")]
    public GameObject[] prefabs;
    public int initailSize = 300;
    private int randombubbleIndex;

    private Queue<GameObject> m_pool = new Queue<GameObject>();

    public void ReUse(Vector3 position, Quaternion rotation)
    {
        if (m_pool.Count > 0)
        {
            GameObject reuse = m_pool.Dequeue();//如果東西大於0，則把裡面的東西拿出來(拿出Queue)
            reuse.transform.position = position;
            reuse.transform.rotation = rotation;
            reuse.SetActive(true);
        }
        else if (m_pool.Count <= 0)//小於0代表沒東西了，生成泡泡
        {
            randombubbleIndex = Random.Range(0, prefabs.Length);
            GameObject go = Instantiate(prefabs[randombubbleIndex]) as GameObject;
            go.transform.position = position;
            go.transform.rotation = rotation;
        }
    }


    public void Recovery(GameObject recovery)
    {
        m_pool.Enqueue(recovery);//把東西放回Queue，然後設成false，觸發這個的條件寫在其他地方
        recovery.SetActive(false);
    }
}
