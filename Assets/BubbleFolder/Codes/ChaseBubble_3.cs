﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SA;

namespace Bubble
{
    
    public class ChaseBubble_3 : MonoBehaviour
    {
        //public FightSystem fightSystem;
        //public GameObject go_FightSystem;
        //public AI_data data;
        //public GameObject go_data;

        public GameObject controller;
        public ChaseBubblePool chaseBubblePool;
        public BubbleHitEffectPool bubbleHitEffectPool;

        public BubbleHitGroundEffectPool bubbleHitGroundEffectPool;

        // Start is called before the first frame update
        private void Awake()
        {
            //go_FightSystem = GameObject.Find("FightSystem");
            //fightSystem = go_FightSystem.GetComponent<FightSystem>();

            //go_data=this.gameObject;
            //data=go_data.GetComponent<AI_data>(); 
            ColorChange();

            controller = GameObject.FindGameObjectWithTag("Controller");
            chaseBubblePool = GameObject.FindGameObjectWithTag("ObjectPool").GetComponent<ChaseBubblePool>();
            bubbleHitEffectPool = GameObject.FindGameObjectWithTag("ObjectPool").GetComponent<BubbleHitEffectPool>();
            bubbleHitGroundEffectPool = GameObject.FindGameObjectWithTag("ObjectPool").GetComponent<BubbleHitGroundEffectPool>();
            //瞄準玩家的隨機補償角度，不要每顆都瞄準玩家位置，太死板
            rotateX = Random.Range(-2.50f, 2.50f);
            rotateY = Random.Range(-2.50f, 2.50f);
            rotateZ = Random.Range(-2.5f, 2.50f);

            //隨機初始速度，之後會規0
            bubbleSpeed = Random.Range(3, 5);
            //隨機衝刺速度
            bubbleSpeedUpSpeed = Random.Range(8, 16);
            //隨機的停頓時間
            bubbleStopWaitTime = Random.Range(1.0f, 3.0f);

        }

        /// <summary>
        /// 回到物件池時初始化
        /// </summary>
        void Init()
        {
            //計時器歸0
            bubbleStopTimer = 0f;
            //瞄準玩家的隨機補償角度，不要每顆都瞄準玩家位置，太死板
            rotateX = Random.Range(-2.50f, 2.50f);
            rotateY = Random.Range(-2.50f, 2.50f);
            rotateZ = Random.Range(-2.5f, 2.50f);

            //隨機初始速度，之後會規0
            bubbleSpeed = Random.Range(3, 5);
            //隨機衝刺速度
            bubbleSpeedUpSpeed = Random.Range(8, 16);
            //隨機的停頓時間
            bubbleStopWaitTime = Random.Range(1.0f, 3.0f);
        }

        void Start()
        {
            
        }

        private void FixedUpdate()
        {
            //BubbleLife();
            //BubbleMove();
            BubbleLifeReturnToPool();
            

            
            
                BubbleSpeedUpMove();
          
                BubbleSlowDownMove();
            BubbleHitGround();
        }

        #region BubbleColor
        Color[] _color;
        public void ColorChange()
        {
            _color = new Color[100];
            
            for (int i=0; i<_color.Length; i++)
            {
                _color[i].r = Random.Range(0.0f, 1.0f);
                _color[i].g = Random.Range(0.0f, 1.0f);
                _color[i].b = Random.Range(0.0f, 1.0f);
                this.transform.GetComponent<MeshRenderer>().material.SetColor("_base_color", _color[i]);
            }

            

        }

        #endregion

        #region Bubble Life
        void OnEnable()//物件被Active的時候紀錄物件當時的情況
        {
            _timer = Time.time;//開始計時
        }

        [Header("Bubble Life Settings")]
        public float _timer;
        public float bubbleLife = 10.0f;

        void BubbleLife()//原版的泡泡生命，生命沒了直接Destory自己
        {
            bubbleLife -= Time.deltaTime;//泡泡的生命，持續時間
            if (bubbleLife < 0) { Destroy(gameObject); }//當時間<0時，泡泡消滅
        }

        void BubbleLifeReturnToPool()//新版的回到物件池的泡泡
        {
            //下面這段if，應該只是防呆，有測試過如果刪掉還是一樣可以執行
            if (!gameObject.activeInHierarchy)
            {
                //Awake();
                return;
            }
            //

            if (Time.time > _timer + bubbleLife)//如果現在的遊戲時間大於 物件被Active時的時間+泡泡生命時，把泡泡回收
            {
                
                Init();
                chaseBubblePool.Recovery(this.gameObject);
            }
        }

        public void BubbleHitGround()
        {
            if (this.transform.position.y <= controller.transform.position.y + 0.2f && this.transform.position.y >= controller.transform.position.y - 0.2f)
            {
                bubbleHitGroundEffectPool.ReUse(this.gameObject.transform.position, Quaternion.Euler(0.0f, 0.0f, 0.0f));
                chaseBubblePool.Recovery(this.gameObject);
            }
        }
        #endregion

        #region Bubble Move
        [Header("Bubble Transform Settings")]
        public float bubbleSpeed = 5.0f;
        private float bubbleDecideSpeed;
        public float bubbleSlowDownSpeed=0.1f;
        public float bubbleStopTimer;
        public float bubbleSpeedUpSpeed;
        public float bubbleStopWaitTime;//泡泡的停頓時間
        void BubbleMove()
        {
            this.transform.position += this.transform.forward * /*這邊控制泡泡的速度*/bubbleSpeed * Time.deltaTime;
        }


        void BubbleSlowDownMove()
        {
            this.transform.position += this.transform.forward * bubbleSpeed * Time.deltaTime;
            bubbleSpeed -= bubbleSlowDownSpeed;
            if (bubbleSpeed < 0)
            {
                bubbleSpeed = 0;
                bubbleStopTimer += Time.deltaTime;
                //b_allowAiming = true;
                BubbleAimingControllerPlusRandom();
            }
        }

        void BubbleSpeedUpMove()
        {
            if (bubbleStopTimer > bubbleStopWaitTime)
            {
                bubbleSpeed = bubbleSpeedUpSpeed;
                //b_allowAiming = false;
                return;
            }
            this.transform.position += this.transform.forward * bubbleSpeed * Time.deltaTime;
            bubbleSpeed += 0.01f;
        }
        #endregion

        #region BubbleRotation
        [Header("Bubble Rotation Settings")]
        public float aimingRotationSpeed=0.5f;
        private float rotateX;
        private float rotateY;
        private float rotateZ;
        public bool b_allowAiming; 

        public void BubbleAimingControllerPlusRandom()
        {
            //if (b_allowAiming ==false) { return; }

            //Vector3 dirToPlayer = controller.transform.position - this.transform.position;
            Vector3 dirToPlayer = new Vector3(controller.transform.position.x - transform.position.x, (controller.transform.position.y - transform.position.y) + 0.25f, controller.transform.position.z - transform.position.z);
            Quaternion lookRotation = Quaternion.LookRotation(dirToPlayer);
            
            //下面這兩個產生隨機角度
            Quaternion randomAngle = Quaternion.Euler(rotateX, rotateY, rotateZ);
            Quaternion newAngle = lookRotation * randomAngle;

            this.transform.rotation = Quaternion.Slerp(this.transform.rotation, newAngle.normalized, aimingRotationSpeed);

        }
        #endregion

        #region Bubble Attack

        public bool bubbleHitPlayer=false;

        public void OnTriggerEnter(Collider other)//這邊寫泡泡撞到的東西會怎樣
        {//這邊只寫，泡泡撞到泡泡不會毀滅，泡泡不會互相影響
            //print(other.gameObject.name);
            if (other.gameObject.layer == LayerMask.NameToLayer("Bubble"))
            {
                //不做任何事
            }
            else if (other.gameObject.layer == LayerMask.NameToLayer("Bullet"))//打到子彈圖層，應該還要產生動畫
            {
                Init();
                chaseBubblePool.Recovery(this.gameObject);//回收泡泡，this=泡泡
            }
            else if (other.gameObject.layer == LayerMask.NameToLayer("Player"))//打到玩家圖層
            {
                Init();
                chaseBubblePool.Recovery(this.gameObject);//回收泡泡，this=泡泡
            }
            else if (other.gameObject.layer == LayerMask.NameToLayer("Controller"))//打到玩家圖層
            {
                Init();
                bubbleHitEffectPool.ReUse(this.transform.position, Quaternion.identity);
                chaseBubblePool.Recovery(this.gameObject);//回收泡泡，this=泡泡
            }
            else if (other.gameObject.tag == ("Weapon"))//打到武器圖層
            {
                Init();
                chaseBubblePool.Recovery(this.gameObject);//回收泡泡，this=泡泡
            }
            #endregion
        }
    }
}