﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SA;

namespace Bubble
{
    public class ChaseBubble_1 : MonoBehaviour
    {
        //public FightSystem fightSystem;
        //public GameObject go_FightSystem;
        //public AI_data data;
        //public GameObject go_data;
        
        // Start is called before the first frame update
        private void Awake()
        {
            //go_FightSystem = GameObject.Find("FightSystem");
            //fightSystem = go_FightSystem.GetComponent<FightSystem>();

            //go_data=this.gameObject;
            //data=go_data.GetComponent<AI_data>(); 
        }

        void Start()
        {
            controller = GameObject.FindGameObjectWithTag("Controller");//初始抓玩家物件
            bubbleSpeed = Random.Range(bubbleSpeedRandomMin, bubbleSpeedRandomMax);//初始隨機生成速度
        }

        private void FixedUpdate()
        {
            //BubbleLife();
            //BubbleMove();
            BubbleLifeReturnToPool();
            BubbleChaseMove();
        }

        #region Bubble Life
        void OnEnable()//物件被Active的時候紀錄物件當時的情況
        {
            _timer = Time.time;//開始計時
        }

        [Header("Bubble Life Settings")]
        public float _timer;
        public float bubbleLife = 10.0f;

        void BubbleLife()//原版的泡泡生命，生命沒了直接Destory自己
        {
            bubbleLife -= Time.deltaTime;//泡泡的生命，持續時間
            if (bubbleLife < 0) { Destroy(gameObject); }//當時間<0時，泡泡消滅
        }

        void BubbleLifeReturnToPool()//新版的回到物件池的泡泡
        {
            //下面這段if，應該只是防呆，有測試過如果刪掉還是一樣可以執行
            if (!gameObject.activeInHierarchy)
                return;

            if (Time.time > _timer + bubbleLife)//如果現在的遊戲時間大於 物件被Active時的時間+泡泡生命時，把泡泡回收
            {
                GameObject.FindWithTag("BubblePool").GetComponent<BubblePool>().Recovery(this.gameObject);
            }
        }
        #endregion

        #region Bubble Move
        [Header("Bubble Transform Settings")]
        public float bubbleSpeed = 4.0f;
        public GameObject controller;
        public int bubbleSpeedRandomMin;
        public int bubbleSpeedRandomMax;
        private Vector3 controllerSuppourtVectorY= new Vector3(0,0.75f,0);
        private Quaternion angleToPlayer;
        private Vector3 dirToPlayer;
        void BubbleMove()
        {
            this.transform.position += this.transform.forward * /*這邊控制泡泡的速度*/bubbleSpeed * Time.deltaTime;

            //if (Vector3.Distance(this.transform.position, controller.transform.position) < 0.5f)
            //{
            //    this.transform.position = Vector3.Slerp(this.transform.position, controller.transform.position + controllerSuppourtVectorY, bubbleSpeed * 0.008f);
            //}
        }
        void BubbleChaseMove()//會追到玩家的位置的位移
        {
            //Vector3 dirToplayer= controller.transform.position- this.transform.position;

            //this.transform.position = Vector3.Lerp(this.transform.position, controller.transform.position + controllerSuppourtVectorY, bubbleSpeed * 0.005f);
            //0.00f 是避免讓球的速度太快的緩衝數值

            dirToPlayer = (controller.transform.position+ controllerSuppourtVectorY) - this.transform.position;

            Quaternion lookAtPlayer = Quaternion.LookRotation(dirToPlayer, Vector3.up);
            
            this.transform.rotation= Quaternion.Slerp(this.transform.rotation.normalized, lookAtPlayer, bubbleSpeed);

            this.transform.position += this.transform.forward *bubbleSpeed*0.4f * Time.deltaTime;// /*這邊控制泡泡的速度*/

        }
        #endregion


        #region Bubble Attack

        public bool bubbleHitPlayer=false;

        public void OnTriggerEnter(Collider other)//這邊寫泡泡撞到的東西會怎樣
        {//這邊只寫，泡泡撞到泡泡不會毀滅，泡泡不會互相影響
            //print(other.gameObject.name);
            if (other.gameObject.layer == LayerMask.NameToLayer("Bubble"))
            {
                //不做任何事
            }
            else if (other.gameObject.layer == LayerMask.NameToLayer("Bullet"))//打到子彈圖層，應該還要產生動畫
            {
                GameObject.FindWithTag("BubblePool").GetComponent<BubblePool>().Recovery(this.gameObject);//回收泡泡，this=泡泡
            }
            else if (other.gameObject.layer == LayerMask.NameToLayer("Player"))//打到玩家圖層
            {
                GameObject.FindWithTag("BubblePool").GetComponent<BubblePool>().Recovery(this.gameObject);//回收泡泡，this=泡泡
            }
            else if (other.gameObject.layer == LayerMask.NameToLayer("Controller"))//打到玩家圖層
            {
               
                GameObject.FindWithTag("BubblePool").GetComponent<BubblePool>().Recovery(this.gameObject);//回收泡泡，this=泡泡
            }
            else if (other.gameObject.tag == ("Weapon"))//打到武器圖層
            {
                
                GameObject.FindWithTag("BubblePool").GetComponent<BubblePool>().Recovery(this.gameObject);//回收泡泡，this=泡泡
            }
            #endregion
        }
    }
}