﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Bubble;

public class BubbleHitEffect : MonoBehaviour
{

    public BubbleHitEffectPool bubbleHitEffectPool;
    // Start is called before the first frame update
    void Start()
    {
        bubbleHitEffectPool = GameObject.FindGameObjectWithTag("ObjectPool").GetComponent<BubbleHitEffectPool>();
    }

    private void OnEnable()
    {
        _timer = Time.time;
    }

    // Update is called once per frame
    void Update()
    {
        EffectLifeReturnToPool();
    }

    public float _timer;
    public float effectLife;

    void EffectLifeReturnToPool()//新版的回到物件池的泡泡
    {
        //下面這段if，應該只是防呆，有測試過如果刪掉還是一樣可以執行
        if (!gameObject.activeInHierarchy)
            return;

        if (Time.time > _timer + effectLife)//如果現在的遊戲時間大於 物件被Active時的時間+泡泡生命時，把泡泡回收
        {
            bubbleHitEffectPool.Recovery(this.gameObject);
        }
    }
}
