﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SA
{
    public class FollowMachine_4 : MonoBehaviour
    {
        //找到所有敵人
        public GameObject[] allEnemies;
        public float distanceToMachine;
        public GameObject controller;
        public FightSystem fightSystem;
        //跟隨時漂浮、轉向
        [Header("Floating Settings")]
        public float rangeOfArc = 0; //計算用，會一直加
        public float perRangeOfArc = 0.03f; //每秒漂浮變動量
        public float radius = 0.45f;//上下漂浮的範圍

        //跟隨
        [Header("Following Settings")]
        public GameObject player; //跟隨的目標
        private Vector3 tempVec = Vector3.zero; //輔助用向量
                                                /*下面三個調整機器與主角之間的距離*/
        public float followXPoint = -0.72f;
        public float followYPoint = 2f;
        public float followZPoint = -0.50f;

        [Header("Other Settings")]
        public bool onAttacking = false; //是否攻擊狀態
        public float bulletTimer = 0.2f;//子彈發射的倒數計時器，設定用
        public float bulletCountDown;//子彈發射的倒數計時，觀察用
        public float emitterCountDown;//槍口火光倒數計時，觀察用
        public float emitterTimer = 0.2f;//槍口火光倒數計時，設定用

        [Header("AutoAiming")]
        public Ray aimingRay;
        public RaycastHit beHitObject;
        public GameObject beHitEnemy;
        public Vector3 v_beHitEnemyPosition;
        public AI_data beHitEnemy_data;
        public AI_data boss_data;

        [Header("Ball At Camera Center Settings")]
        public RaycastHit hitTemp;
        public GameObject ballTarget;

        //射擊
        [Header("Shooting Settings")]
        public GameObject machineBullet; //子彈
        public Transform machineEmitter; //子彈發射口
        public Transform ballAtCameraCenter; //一直掛在相機中間的
        public GameObject emitterFire; //槍口的火光

        /*下面這三個影響到射擊時與主角的位置*/
        public float shootPositionX = 0.0f;
        public float shootPositionY = 2.0f;
        public float shootPositionZ = -0.5f;
        public BulletPool pool;//子彈的物件池(是一個空物件搭載了 BulletPool的code)
        public bool autoShoot;
        public float autoShootPositionTimer;
        public float leaveAutoShootRange;


        private void Awake()
        {
            ////自動找發射子彈對準的準心
            //ballAtCameraCenter = GameObject.Find("center").transform;
            //ballTarget = GameObject.Find("Ball Target");

            allEnemies = GameObject.FindGameObjectsWithTag("Enemy");
            controller = GameObject.FindGameObjectWithTag("Controller");
            fightSystem = GameObject.FindGameObjectWithTag("FightSystem").GetComponent<FightSystem>();
            boss_data = GameObject.Find("Boss").GetComponent<AI_data>();
        }
        
        // Start is called before the first frame update
        void Start()
        {
            //一開始要先讓槍口火光(粒子系統)false看不到
            emitterFire.SetActive(false);

            ballTarget.transform.position = ballAtCameraCenter.transform.position;
            pool = GameObject.FindGameObjectWithTag("ObjectPool").GetComponent<BulletPool>();

            //aimingRay= Camera.main.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0.0f));
            //aimingRay = new Ray(controller.transform.position+new Vector3(0,0.5f,0) , controller.transform.position + new Vector3(0, 0.5f, 0) + controller.transform.forward );
        }

        // Update is called once per frame
        void FixedUpdate()
        {

            if (Input.GetKeyDown(KeyCode.L))
            {
                leaveAutoShootRange = 0;
            }
            else if (Input.GetKeyDown(KeyCode.L))
            {
                leaveAutoShootRange = 10;
            }
            //if (Input.GetKeyDown(KeyCode.L))
            //{
            //    autoShoot = true;
            //}          
            //aimingRay = new Ray(controller.transform.position + new Vector3(0, 0.5f, 0), /*controller.transform.position + new Vector3(0, 0.5f, 0) */ controller.transform.forward);
            InFollowState();
            
            //EnemyDistanceToMachine();

            ShootWithBulletsPool();
            //ChangeBallPosition();

            

            //抓腳色面前的射線
            BeHitEnemy();
            //抓腳色前面怪獸的血量
            GetEnemyData();

            //自動射擊
            FollowMachineAutoShootAI();
            AutoShoot();
            CheckControllerDead();
        }

        private void OnDrawGizmos()
        {
            Gizmos.color = Color.cyan;
            Gizmos.DrawRay(Camera.main.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0.0f)));
        }

        //平常沒射擊時的位置及鎖定在相機正中間
        public void InFollowState()
        {
            if (onAttacking) { return; }
            rangeOfArc += perRangeOfArc;//一直增加的值
            float dy = Mathf.Cos(rangeOfArc) * radius; //用三角函數讓他那個值在某區間浮動

            //跟隨目標的向量再加一個位置
            Vector3 temp = new Vector3(followXPoint, dy + followYPoint, followZPoint) + player.transform.position;
            // y座標是會變動的，所以跟隨的位置也會變動
            //柔軟的跟著目標(跟隨相機)
            transform.position = Vector3.SmoothDamp(transform.position, temp, ref tempVec, 0.5f);
            //如果非攻擊狀態，同player的rotation

            FollowAngleMode();
        }

        //平常沒射擊時的位置及鎖定在相機正中間
        protected void FollowAngleMode()
        {
            if (onAttacking == false)//false的時候抓player的rotation
            {
                this.transform.rotation = player.transform.rotation;
            }
            else//true的時候瞄準標靶的那顆球
            {
                Vector3 targetAngle = ballTarget.transform.position - this.transform.position;
                this.transform.rotation  = Quaternion.LookRotation(targetAngle);
            }
        }
        
        //射線自動尋找敵人
        public GameObject BeHitEnemy()
        {
            //Debug.DrawRay(controller.transform.position + new Vector3(0, 0.5f, 0), controller.transform.forward*30f, Color.green);
            if(Physics.Raycast(controller.transform.position + new Vector3(0, 0.5f, 0), controller.transform.forward, out beHitObject, 30f))
            {
                //Debug.Log(beHitObject.transform.gameObject.name);
                if (beHitObject.collider.tag == "Enemy")
                {

                    
                    beHitEnemy = beHitObject.transform.gameObject;
                    v_beHitEnemyPosition = beHitObject.collider.bounds.center;
            }
        }
            return beHitEnemy;
        }

        //沒用到，所有敵人與機器的距離
        private void EnemyDistanceToMachine()
        {
            for (int i = 0; i < allEnemies.Length; i++)
            {
                distanceToMachine = Vector3.Distance(allEnemies[i].transform.position, this.transform.position);
            }
        }

        //自動瞄準，用在自動射擊裡
        public void AutoAiming()
        {
            if (!onAttacking) { this.transform.rotation = player.transform.rotation; }
            else
            {
                //if (beHitEnemy == null)
                //{
                //    this.transform.rotation = Quaternion.LookRotation(ballTarget.transform.position - this.transform.position);
                //}
                //else
                //{
                //Vector3 randomVector = new Vector3(Random.Range(0.1f, 0.3f), Random.Range(0.1f, 0.3f), Random.Range(0.1f, 0.3f));
                    this.transform.rotation = Quaternion.LookRotation( (v_beHitEnemyPosition - this.transform.position )+ new Vector3(0, Mathf.Sin(autoShootPositionTimer) * 0.5f, 0));
                //}
                
            }


        }

        //裝在攝影機上的球的位置
        public void ChangeBallPosition()
        {
            //Ray bulletShootLine = new Ray(machineEmitter.transform.position, ballAtCameraCenter.transform.position);//從子彈發射口到攝影機中央的球的射線
            Ray viewPortRay = Camera.main.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0.0f));//從畫面viewPort射出去的射線，0.5是因為viewPort的(x,y)最大值只有1而已
            if (Physics.Raycast(viewPortRay, out hitTemp, 100f))//射線打到東西，100f是距離
            {
                if (hitTemp.collider.tag == "Enemy")//且那個東西的tag是Enemy (所以泡泡的tag也要設定成Enemy)
                {
                    ballTarget.transform.position = hitTemp.point;//瞄準的位置球=那個敵人的位置(自動瞄準)
                }
            }
            else
            {
                ballTarget.transform.position = ballAtCameraCenter.transform.position;//如果射線沒打到東西，瞄準位置的球=攝影機中心的球
            }

        }

        //自動射擊的AI
        public void FollowMachineAutoShootAI()
        {
            
            if (beHitEnemy==null ||  boss_data.enemyTarget.currentState == StateType.LastBossDeath)
            {
                autoShoot = false;
                Debug.Log("No Enemy");
            }            
            else if(beHitEnemy!=null && beHitEnemy_data.currentHp >0)
            {
                autoShoot = true;
                Debug.Log("Found Enemy, AutoShoot=true");
                
                if (Vector3.Distance(beHitEnemy.transform.position, this.transform.position) > leaveAutoShootRange)
                {
                    autoShoot = false;
                    Debug.Log("Found Enemy, but too far");
                }
            }
        }

        //自動射擊
        public void AutoShoot()
        {
            if (!autoShoot) { return; }
            bulletCountDown -= Time.deltaTime;
            autoShootPositionTimer += Time.deltaTime;

            //取得被打怪獸之資料
            beHitEnemy_data = beHitEnemy.GetComponent<AI_data>();

            //進入攻擊狀態
            onAttacking = true;
            //FollowAngleMode();//攻擊時的角度跟隨

            //射擊時，讓火光的粒子系統true
            emitterFire.SetActive(true);

            if (beHitEnemy==null)
            {
                FollowAngleMode();
            }
            else
            {
                //v_beHitEnemyPosition = new Vector3(Random.Range(0.1f, 0.3f), 0.5f, Random.Range(0.1f, 0.3f));
                
                AutoAiming();
            }
            


            //跟隨機的位置
            Vector3 shootPosition = player.transform.position + new Vector3(shootPositionX, shootPositionY, shootPositionZ);//這邊這個new向量影響漂浮機攻擊時的位置與主角之間的位置          
            Vector3 shootPosition2 = player.transform.position + new Vector3( -shootPositionX , shootPositionY,  -shootPositionZ );
            //即使是順移到位置，射擊時也還是會有超小幅度的上下位移
            if (beHitEnemy == null)
            {
                transform.position = Vector3.Slerp(this.transform.position, shootPosition, 0.5f);
            }
            else 
            {
                transform.position = Vector3.Slerp(this.transform.position, shootPosition2, 1.0f);
                //if (Vector3.Distance(beHitEnemy.transform.position, this.transform.position) <2.0f)
                //{

                //}
                //if (autoShootPositionTimer> 5.0f && autoShootPositionTimer < 10f)
                //{
                //    transform.position = Vector3.Slerp(this.transform.position, shootPosition2, 1.0f);
                    
                //}
                //else if (autoShootPositionTimer >10f)
                //{
                //    transform.position = Vector3.Slerp(this.transform.position, shootPosition, 1.0f);
                //    autoShootPositionTimer = 0;
                //}
            }

            //子彈發射
            if (bulletCountDown < 0)//小於0才可以發射一顆子彈，不然子彈會黏成一條線
            {
                pool.ReUse(machineEmitter.transform.position, machineEmitter.transform.rotation);//使用BulletPool的方法ReUse，把SetActive(false)的物件拿出來變true
                
                bulletCountDown = bulletTimer;//重新設定時間
            }


            //離開AutoShoot
            if (beHitEnemy==null)
            {
                
            }
            else
            {
                if( Vector3.Distance(beHitEnemy.transform.position, this.transform.position) > leaveAutoShootRange 
                    || beHitEnemy_data.currentHp <=0)
                {
                    autoShoot = false;
                }
            }
            
        }

        //按ctrl鍵的射擊
        public void ShootWithBulletsPool()
        {
            if (autoShoot) { return; }
            
            bulletCountDown -= Time.deltaTime;//倒數計時
            if (Input.GetKey(KeyCode.LeftControl) || Input.GetButton("A"))
            {

                Vector3 shootPosition = player.transform.position + new Vector3(shootPositionX, shootPositionY, shootPositionZ);//這邊這個new向量影響漂浮機攻擊時的位置與主角之間的位置
                transform.position = Vector3.Slerp( transform.position, shootPosition, 0.5f);
                //即使是順移到位置，射擊時也還是會有超小幅度的上下位移
                
                //射擊時，瞄準那顆球
                onAttacking = true;
                FollowAngleMode();

                if (bulletCountDown < 0)//小於0才可以發射一顆子彈，不然子彈會黏成一條線
                {
                    pool.ReUse(machineEmitter.transform.position, machineEmitter.transform.rotation);//使用BulletPool的方法ReUse，把SetActive(false)的物件拿出來變true
                    
                    bulletCountDown = bulletTimer;//重新設定時間
                }

                //射擊時，讓火光的粒子系統true
                emitterFire.SetActive(true);
            }
            else
            {
                //射擊鍵不按的時候，槍口火光變false
                emitterFire.SetActive(false);
                onAttacking = false;
            }

        }




        public void MakeBullet()//舊的製造子彈，會一直一直製造子彈
        {
            bulletCountDown -= Time.deltaTime;//倒數計時
            if (bulletCountDown < 0)//小於0才可以發射一顆子彈
            {
                GameObject MachineBulletClone = Instantiate
                    (machineBullet,
                    machineEmitter.transform.position,
                    machineEmitter.transform.rotation) as GameObject;

                bulletCountDown = bulletTimer;//重新設定時間
            }
        }

        public void Shoot()//舊的射擊，參考用
        {
            if (Input.GetKey(KeyCode.LeftAlt) || Input.GetButton("A"))
            {

                Vector3 shootPosition = player.transform.position + new Vector3(shootPositionX, shootPositionY, shootPositionZ);//這邊這個new向量影響漂浮機攻擊時的位置與主角之間的位置
                transform.position = shootPosition;
                //即使是順移到位置，射擊時也還是會有超小幅度的上下位移

                //射擊時，瞄準那顆球
                onAttacking = true;
                FollowAngleMode();

                //射擊時，讓火光的粒子系統true
                emitterFire.SetActive(true);

                //生出子彈
                MakeBullet();
            }
            else
            {
                //射擊鍵不按的時候，槍口火光變false
                emitterFire.SetActive(false);
            }
        }


        public void CheckControllerDead()
        {
            if(fightSystem.currentHp <= 0)
            {
                leaveAutoShootRange = 0;
            }
        }

        public void GetEnemyData()
        {
            if (beHitEnemy==null) { return; }            
            beHitEnemy_data = beHitEnemy.GetComponent<AI_data>();
        }


        

    }
    

    

}
