﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace SA
{
    public class Gun_turret : MonoBehaviour
    {
        public bool isTower;
        public GameObject controller;
        public  AI_data data;
        public Transform pivot;
        public float turretSpeed = 2f ;
        public Animator anim;
        void Start()
        {
            controller = GameObject.Find("Controller");
            data = gameObject.transform.parent.GetComponentInParent<AI_data>();
            //enemyTarget = gameObject.transform.parent.GetComponentInParent<>
            anim = GetComponent<Animator>();
        }

        // Update is called once per frame
        void Update()
        {
            TurretRotation();
            
        }
        void TurretRotation()
        {
            if(data.currentHp > 0)
            {
                //controller = GameObject.Find("Controller");
                //transform.position += ()
               
                var rotation = Quaternion.LookRotation(controller.transform.position - transform.position, Vector3.up);
                transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * turretSpeed);
                //anim.SetBool("chase", true);
            }
            else
            {
                //transform.Translate(0, 0, 0);
                //anim.SetBool("dead", true);
                this.enabled = false;
            }

             if (isTower == true)
            {
                if (data.currentHp > 0)
                {
                    //controller = GameObject.Find("Controller");
                    //transform.position += ()

                    var rotation = Quaternion.LookRotation(controller.transform.position - transform.position, Vector3.right);
                    transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * turretSpeed);
                    //anim.SetBool("chase", true);
                }

            }
        }

    }
}

