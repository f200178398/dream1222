﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace SA
{
    
    public class FightSystem : MonoBehaviour
    {
        //public static FightSystem m_Instance;

        [Header("加速器")]
        public bool changeTime;
        public float timeSpeed;
        [Header("血量")]
        public GameObject Controller;
        public float maxHp;
        public float currentHp ;
        public bool isDead;
        [Header("傷害")]
        public float damage = 200f;
        public int[] damages;
        public float machineDamage;
        [Header("傷害加成結果")]
        public float totalDamage;
        [Header("能量")]
        public float maxPower;
        public float currentPower;

        public AI_data data;
        public StateManager state;
        public EnemyTarget enemyTarget;//敵人狀態
        public Health itemHealth;
        public Power itemPower;
        public OnTrigger onTrigger;
        [Header("敵人資訊")]
        public Collider enemyCol;
        public GameObject enemyObj; //ray
        public float enemysDamage;
        public float enemyHp;
        public float enemyDamage;
        
        [Header("攻擊中")]
        public bool attack;

        [Header("被敵人擊中")]
        public bool underAttack;

        //public Collider col;
        [Header("武器資訊")]
        public GameObject weapeon;
        public Collider weapCol; //武器碰撞

        [HideInInspector]
        public RaycastHit targetHit;
        public Ray controllerRay;

        [Header("Enemy_data")]
        public AI_data ai_data;
        private float minHp = 0;

        public GameObject[] enemys;
        public float enemysHp;
        public int enemysCount;
        public List<Collider> enemyList;
        public float beRepair;

        [Header("場景")]
        SceneManger SceneManger;
        [Header("動畫觸發器Event")]
        public AttackEvent attackEvent;

        public void Awake()
        {
            //m_Instance = this;
        }
        // Start is called before the first frame update
        void Start()
        {
            ///// ********** 如果只有在Start 呼叫其他程式的資料 ,  那事件觸發之後的敵人就可能無法參考???  有待確認
            data = GameObject.FindWithTag("Enemy").GetComponent<AI_data>();
            currentHp = maxHp;
            currentPower = 0;
            state = GameObject.Find("Controller").GetComponent<StateManager>();
            weapeon = GameObject.Find("Weapon_point");  //先抓到主物件以外的武器物件
            
            weapeon.SetActive(false); // 一開始武器不在手上
            weapCol = weapeon.GetComponent<Collider>();  //先存入武器的碰撞器
            weapCol.enabled = !enabled;  //武器碰撞先關閉
            //Debug.Log(StateManager.Instance().onGround);
            
            Controller = GameObject.Find("Controller"); //在遊戲開始便通知系統找到主角位置
            
            enemys = GameObject.FindGameObjectsWithTag("Enemy"); //將所有的"Enemy" 存入"陣列"
            

            enemysCount = GameObject.FindGameObjectsWithTag("Enemy").Length; //查找所有的敵人數量

            enemyTarget = GameObject.FindGameObjectWithTag("Enemy").GetComponent<EnemyTarget>();

            onTrigger = Controller.GetComponent<OnTrigger>(); //觸發被敵方攻擊的碰撞體

            itemHealth = GameObject.FindGameObjectWithTag("Health").GetComponent<Health>();

            SceneManger = GameObject.Find("SceneManger").GetComponent<SceneManger>();
            attackEvent = Controller.GetComponent<AttackEvent>();
        }


        private void FixedUpdate()
        {
            Power();
            ChangTime();
        }
        void Update()
        {
            EnemysDamage();
            Controller_Ray();
            //BeRepair();
            TakeDamage();
            UpGradeDamage();

            SceneManger.StartCoroutine(SceneManger.ElevatorRise());
            SceneManger.ZoneOneSetActive();
            SceneManger.StartCoroutine(SceneManger.ColseWallCol());
            SceneManger.StartCoroutine(SceneManger.EleveatorDoorDown());
            SceneManger.StartCoroutine(SceneManger.ElevatorDoorUp());
            isWarpAttack();
            CheckControllerDeadAndSpeedTurnZero();// 檢測 主角死亡，速度歸0

            if (attack == true && data.currentHp > 0)
            {
                print("命中敵人");
            }
            else { }

            
        }
        public GameObject GetController()
        {
            return Controller;
        }
        public void TakeDamage() 
        {
            currentHp = onTrigger.currentHp;
            
            if (currentHp <= 0) 
                {
                    currentHp = 0;
                    isDead = true;
                    print("you are dead !!"); 
                }
           
        }

        
        public void Controller_Ray() //抓到 角色前方的敵人COllider   
        {
            controllerRay = new Ray(Controller.transform.position, Controller.transform.position + Vector3.forward); //指定 腳色 RAY位置
            if (Physics.Raycast(controllerRay, out targetHit, 30f)) //RAY 偵測並抓到敵
            {
                if (targetHit.collider.tag == "Enemy")
                {
                    
                    enemyCol = targetHit.collider;
                    enemyObj = enemyCol.gameObject;
                }
            }
            else  //當RAY 沒抓到敵人時返回NULL
            {
                enemyObj = null;
                enemyCol = null;
            }

        }
        /// <summary>
        /// 這裡用來提升主角傷害  必須考慮連擊數 和 能量 的加成
        /// </summary>
        /// <returns></returns>
        public float UpGradeDamage()
        {
            // 能量調會隨時間減少 , 但擊殺敵人後會持續一段時間 (未完成
            // 目前就算揮空還是會增加攻擊力 需改成擊中敵人後增加
            

            float combot = state.attack_count; //連擊 會提高傷害
            totalDamage = damage * combot + (Power() * combot) * combot;  // 傷害公式
            //print("傷害加總 : " + totalDamage + " 當前能量 : " + Power());
            return totalDamage;
        }
        /// <summary>
        /// 能量隨時間慢慢減少
        /// </summary>
        public float Power() 
        {
            float power = currentPower;
            //currentPower += Time.time * 0.01f; // 是否因為UPDATE 而快速增加
            if(attack == true /*&& currentPower>=0*/ && data.currentHp>0)
            {
                currentPower += 40f;
                attack = false;
            }
            else
            {
                currentPower += 0;
            }
            if(currentPower <= 0)
            {
                currentPower = 0;
            }
            else if(currentPower >= maxPower)
            {
                currentPower = maxPower;
            }
            
            return currentPower;
        }

        public float GetHealthRate()
        {
            return currentHp / maxHp;
        }
        public float GetPowerRate()
        {
            return currentPower / maxPower;
        }

        public void BeRepair() // 目前補血後 敵人攻擊仍會以原本血量去扣寫
        { 
            //if (onTrigger.ishealth == true && currentHp > 0)
            //{
            //    currentHp += 500f;
            //    if (currentHp >= maxHp)
            //    {
            //        currentHp = maxHp;
            //    }
            //}
            //else
            //{
            //    currentHp += 0;
            //}
            //onTrigger.ishealth = false;
        }
        //public void GetPower()
        //{
        //    if (itemPower.getPower == true)
        //    {
        //        currentPower += itemPower.power;
        //        if(currentPower >= maxPower)
        //        {
        //            currentPower += 0;
        //        }
        //    }
        //    else { }
        //}
        public void EnemysDamage()
        {
            enemyDamage = data.int_MelleAttack_Damage;
        }

        public void ChangTime()
        {
            if (Input.GetKeyDown(KeyCode.N))
            {
                Time.timeScale = timeSpeed;
            }

            else if (Input.GetKeyDown(KeyCode.M))
            {
                Time.timeScale = 1;
            }
        }

        public void isWarpAttack()
        {
            if(attackEvent.isWarp == true)
            {
                print("閃現攻擊");
                damage += 400f;
                print(damage);
                attackEvent.isWarp = false;

                damage -= 400f;
            }
        }

        /// <summary>
        /// 檢測FightSystem裡面的血量若歸0時，不能移動旋轉
        /// </summary>
        public void CheckControllerDeadAndSpeedTurnZero()
        {
            if (currentHp <= 0 )
            {
                state.moveSpeed = 0;
                state.runSpeed = 0;
                state.rotateSpeed = 0;
            }
        }
    }

}
