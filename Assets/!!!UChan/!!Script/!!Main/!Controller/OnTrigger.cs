﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace SA
{
    public class OnTrigger : MonoBehaviour
    {
        //從戰鬥系統呼叫 來自敵人的攻擊資訊
        public FightSystem fightSystem;
        public EnemyTarget enemyTarget;
        public float currentHp;
        public bool underAttack;
        public bool ishealth;
        public bool isFallDown;
        public Transform attackBox;
        public float damage;
        public float bubbleDamage;
        public int ontriCount;
        Animator anim;
        
        public void Start()
        {
            anim = GetComponent<Animator>();
            currentHp = fightSystem.maxHp;
        }
        public void Update()
        {           
            Dead(); 
            
        }
        public void FixedUpdate()
        {
           
          StartCoroutine(TimeScale());
            
        }
      
        public void Dead()
        {
            if (currentHp <= 0)
            {
                currentHp = 0;
                print("你已經死了");
            }
            
        }
        IEnumerator TimeScale()
        {
            float percentHp = fightSystem.currentHp / fightSystem.maxHp ;
            if (underAttack == true && percentHp < 0.7f && percentHp > 0)
            {
               //print("血量已經小於50%");
                Time.timeScale = 0.1f;
                underAttack = false;
                yield return new WaitForSeconds(0.3f);
                Time.timeScale = 1f;
            }
            if (underAttack == true && percentHp <= 0 )
            {
                if(currentHp < fightSystem.enemyDamage)
                {
                    //print("被致命一擊");
                    Time.timeScale = 0.1f;
                    underAttack = false;
                    yield return new WaitForSeconds(0.3f);
                    Time.timeScale = 1f;
                }
                
            }         
        }
        public void OnTriggerEnter(Collider other)
        {         
                       
            if (other.gameObject.tag == "Enemy_Attack_BOX")
            {
                ontriCount++;
                underAttack = true;
                
                attackBox = other.transform;// 抓到了敵人的武器
                damage = attackBox.transform.parent.transform.parent.transform.parent.transform.parent.transform.parent.transform.parent.transform.parent.GetComponentInParent<AI_data>().int_MelleAttack_Damage;//需要抓父物件並且GetComponentInParent
                                                                                                                                                                                                          //print("最上層物件: "+ attackBox.transform.parent.transform.parent.transform.parent.transform.parent.transform.parent.transform.parent.transform.parent.name);
                currentHp -= damage; //傷害最好別放在Update
                print("敵人攻擊傷害 : " + damage); //表示有抓到傷害
                anim.SetTrigger("take damageTri");
                underAttack = false;
            }
          
            if (other.gameObject.tag == "Bubble")
            {
                ontriCount++;                             
                currentHp -= 40;
                
                //print("主角血量 : " + currentHp);
                print("被泡泡擊中");
                underAttack = true;
                if (ontriCount % 3 == 0)
                {
                    anim.SetTrigger("take damageTri");
                }
                underAttack = false;
            }

            if (other.gameObject.tag == ("Laser"))
            {
                ontriCount++;
                currentHp -= 50f;
                underAttack = true;
                if (ontriCount % 10 == 0)
                {
                    anim.SetTrigger("take damageTri");
                }
                underAttack = false;
            }

            if(other.gameObject.tag == ("Bomb"))
            {
                ontriCount++;
                currentHp -= 800;

                //print("主角血量 : " + currentHp);
                print("被炸彈擊中");
                underAttack = true;
                if (ontriCount % 3 == 0)
                {
                    anim.SetTrigger("take damageTri");
                }
                underAttack = false;
            }

            if(other.gameObject.tag == ("JumpShockWave"))
            {
                isFallDown = true;
                underAttack = true;
                if (fightSystem.state.onGround)
                {
                    currentHp -= 1000;
                    anim.SetTrigger("FallDown");
                }
               
                underAttack = false;
                
            }

            if(other.gameObject.tag == ("Health"))
            {
                if(fightSystem.currentHp > 0)
                {
                    currentHp += 1000f;
                    if(currentHp > fightSystem.maxHp)
                    {
                        currentHp = fightSystem.maxHp;
                    }
                }
                
            }

            
        }
        public void OnTriggerExit(Collider other)
        {
            //underAttack = false;

            if (other.gameObject.tag == "Enemy_Attack_BOX")
            {
                anim.SetBool("take damage", false);
                underAttack = false;
                
            }           
        }
       
    }
    
}

