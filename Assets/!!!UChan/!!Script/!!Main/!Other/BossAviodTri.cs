﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossAviodTri : MonoBehaviour
{
    public GameObject aviod;
    public TimelineManager timeline;
    void Start()
    {
        timeline = GetComponent<TimelineManager>();
        aviod.SetActive(false);

    }

    
    void Update()
    {
        StartCoroutine(Aviod());
    }
     IEnumerator Aviod()
    {
        if (timeline.timelineTrigger == true)
        {
            yield return new WaitForSeconds(5f);
            aviod.SetActive(true);
        }
        

    }
}
