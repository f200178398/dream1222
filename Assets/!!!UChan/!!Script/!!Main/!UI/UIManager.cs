﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using SA;
using TMPro;
public class UIManager : MonoBehaviour
{
    [Header("Controller UI")]
    public FightSystem fightSystem;
    public Image hpBar;
    public Image hpBarBackGround;
    public GameObject controller;
    public Canvas controllerBar;
    public float c_CurrentHp;

    [Header("Boss UI")]
    public AI_data bossData;
    public GameObject boss;
    public Canvas bossHpCanvas;    
    public Image BossHpBar;
    public float BossCurrentHp;
    public float fillamount;
    float lerpSpeed = 0.2f;
    float lerpspeedSlow = 0.1f;

    [Header("Chat UI")] //腳色對話窗的背景
    public bool chatBGswitch;
    public Image Chat_BG;
    public Canvas chatBG;

    public bool isDialog;
    public TextMeshProUGUI textDisplay;
    public string[] sentences_01;
    public string[] sentences_02;
    private int index_01;
    private int index_02;
    public float typingSpeed;
    public GameObject continueButton;
    public Animator textAnim;

    [Header("觸發對話的區域")]
    public Collider d_tri_01;
    public Dialog_Tri dialog_Tri;
    // Start is called before the first frame update
    void Start()
    {
        fightSystem = GameObject.Find("FightSystem").GetComponent<FightSystem>();
        hpBar = GameObject.Find("C_CurrentHP").GetComponent<Image>();
        hpBarBackGround = GameObject.Find("C_Hp_BG").GetComponent<Image>();
        controller = GameObject.Find("Controller");
        controllerBar = GameObject.Find("ControllerBar").GetComponent<Canvas>();
        bossHpCanvas = GameObject.Find("BossHpBar").gameObject.GetComponent<Canvas>();      
        boss = GameObject.Find("Boss");
        bossData = boss.gameObject.GetComponent<AI_data>();
        BossHpBar = GameObject.Find("BossCurrentHP").GetComponent<Image>();
        bossHpCanvas.gameObject.SetActive(false);

        chatBG = GameObject.Find("Chat_BG_ALL").GetComponent<Canvas>();
        
        dialog_Tri = GameObject.Find("d_tri_01").GetComponent<Dialog_Tri>();

        if (!gameObject.activeInHierarchy  && gameObject.tag == ("Enemy"))
        {
            print(gameObject.name);
        }
        //StartCoroutine(Type());
    }

    // Update is called once per frame
    void Update()
    {
        Contuine();
        StartCoroutine(ControllerHp());
        StartCoroutine(BossHp());
        CloseChatBG();
       
        
        //if(dialog_Tri.d_tri == true)
        //{
        //    print("開始對話");
        //}
    }
    IEnumerator ControllerHp() //這裡用來控制 角色HP
    {
        c_CurrentHp = fightSystem.currentHp;
        float chp = fightSystem.GetHealthRate();
        hpBar.fillAmount = Mathf.Lerp(hpBar.fillAmount, chp, lerpSpeed);

        yield return new WaitForSeconds(3f);
        hpBarBackGround.fillAmount = Mathf.Lerp(hpBar.fillAmount, chp, lerpspeedSlow);

        
    }
    
    IEnumerator BossHp()
    {
        float hp = bossData.GetHealthRate();
        BossHpBar.fillAmount = Mathf.Lerp(BossHpBar.fillAmount, hp, lerpSpeed);
        float dis =  Vector3.Distance(controller.transform.position , boss.transform.position);
        if( dis < 30f && bossData.enemyTarget.currentState != SA.StateType.LastBossDeath)
        {
            bossHpCanvas.gameObject.SetActive(true);
        }
        else if( hp<= 0)
        {
            yield return new WaitForSeconds(3);                     
            bossHpCanvas.gameObject.SetActive(false);     
        }
    }

    /// <summary>
    /// 對話視窗背景
    /// </summary>
    public void CloseChatBG() 
    {
        if( isDialog == true) //如果對話視窗開始
        {
            //hpBar.canvasRenderer.cull = true;
            chatBG.gameObject.SetActive(true);  //開啟對話的背景 (灰色透明)
           
            controllerBar.gameObject.SetActive(false); //並關掉玩家UI
        }
        else
        {
            controllerBar.gameObject.SetActive(true); //開啟玩家UI
            //hpBar.canvasRenderer.cull = false;
            
            chatBG.gameObject.SetActive(false); //關掉對話背景
        }
    }

    public IEnumerator Type()
    {
        isDialog = true;
       
        //if(chatBGswitch == true)
        //{
        foreach (char letter in sentences_01[index_01].ToCharArray())
            {
                textDisplay.text += letter;
                yield return new WaitForSeconds(typingSpeed);
            }
        //}
       
    }
    public IEnumerator Type_02()
    {
        foreach (char letter in sentences_02[index_02].ToCharArray())
        {
            textDisplay.text += letter;
            yield return new WaitForSeconds(typingSpeed);
        }
    }

    public  IEnumerator CloseBG()
    {
        yield return StartCoroutine(Type());
        chatBG.gameObject.SetActive(false);
    }

    public void NextSentence() //到下一句對話內容
    {
        textAnim.SetTrigger("Change");
        continueButton.SetActive(false);
        if(index_01 < sentences_01.Length - 1)
        {
            index_01++;
            textDisplay.text = "";
            StartCoroutine(Type());
            //return;
        }       
        else
        {
            textDisplay.text = "";
            continueButton.SetActive(false);
        }
        // if (index_02 < sentences_02.Length - 1)
        //{
        //    index_02++;
        //    textDisplay.text = "";
        //    StartCoroutine(Type_02());
        //}
        //else
        //{
        //    textDisplay.text = "";
        //    continueButton.SetActive(false);
        //}
    }

    public void Contuine() //按下BUTTON後繼續下一段對話
    {
        if (textDisplay.text == sentences_01[index_01])
        {
            continueButton.SetActive(true);
        }
        else if (textDisplay.text == sentences_02[index_02])
        {
            continueButton.SetActive(true);
        }
    }

    public void OnTriggerEnter(Collider other)
    {
        //dialog_Tri.OnTriggerEnter(other);
    }
}
