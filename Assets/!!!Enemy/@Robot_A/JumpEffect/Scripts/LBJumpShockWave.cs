﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LBJumpShockWave : MonoBehaviour
{
    [Header("Object Pool")]
    public LBJumpShockWavePool LBJumpShockWavePool;
    [Header("Stretch Speed")]
    public float f_stretchSpeed=20.0f;
    [Header("Effect Life")]
    public float effectLifeTime = 1.2f;
    public float _timer;

    // Start is called before the first frame update
    void Start()
    {
        LBJumpShockWavePool = GameObject.FindGameObjectWithTag("ObjectPool").GetComponent<LBJumpShockWavePool>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        ColliderStretch();
        ReturnToPool();
    }

    public void ColliderStretch()
    {
        transform.localScale += new Vector3(f_stretchSpeed, 0.0f, f_stretchSpeed) * Time.deltaTime;
        if (transform.localScale.x >=20f)
        {
            f_stretchSpeed = 0;
        }
    }
    public void OnEnable()
    {
        _timer = Time.time;
        transform.localScale = new Vector3(5f, 0.1f, 5f);
    }

    public void ReturnToPool()
    {
        if (!gameObject.activeInHierarchy) { return; }

        if (Time.time > _timer + effectLifeTime)//如果現在的遊戲時間大於 物件被Active時的時間+泡泡生命時，把泡泡回收
        {
            LBJumpShockWavePool.Recovery(this.gameObject);
        }
    }


}
