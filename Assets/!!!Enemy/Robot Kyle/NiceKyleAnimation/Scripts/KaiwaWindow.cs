﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SA;


public class KaiwaWindow : MonoBehaviour
{
    Camera cameraMain;

    public GameObject go_kaiwaWindow1;
    public GameObject go_kaiwaWindow2;
    public GameObject go_kaiwaWindow3;
    public GameObject go_kaiwaWindow4;

    public bool b_controllerInRange;
    // Start is called before the first frame update
    void Start()
    {
        cameraMain = Camera.main;
    }

    // Update is called once per frame
    void Update()
    {
        FaceToCameraForever();
        //ShowMessage();
    }

    public void FaceToCameraForever()
    {
        this.transform.LookAt(this.transform.position + cameraMain.transform.rotation * Vector3.forward,
           cameraMain.transform.rotation * Vector3.up);
    }
    public void ShowMessage(GameObject kaiwaWindow)
    {
        if (b_controllerInRange)
        {
            kaiwaWindow.gameObject.SetActive(true);
        }
        else
        {
            kaiwaWindow.gameObject.SetActive(false);
        }
    }
}
