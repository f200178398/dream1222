﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace LaserMaker
{
    public class LaserWillShootCircle : MonoBehaviour
    {

        public LaserWillShootPlaceCirclePool laserWillShootPlaceCirclePool;
        // Start is called before the first frame update
        void Start()
        {
            laserWillShootPlaceCirclePool = GameObject.FindGameObjectWithTag("ObjectPool").GetComponent<LaserWillShootPlaceCirclePool>();
        }

        // Update is called once per frame
        void Update()
        {
            LaserWillShootPlaceCircleReturnToPool();
        }

        [Header("這個特效的生命")]
        public float _timer;
        public float lifeTime = 2.5f;

        public void OnEnable()
        {
            _timer = Time.time;
        }

        public void LaserWillShootPlaceCircleReturnToPool()
        {
            if (!gameObject.activeInHierarchy)
                return;

            if (Time.time > _timer + lifeTime)//如果現在的遊戲時間大於 物件被Active時的時間+泡泡生命時，把泡泡回收
            {
                laserWillShootPlaceCirclePool.Recovery(this.gameObject);
            }
        }
    }
}